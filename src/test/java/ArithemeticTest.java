import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArithemeticTest  {

    @Test
    public void equals()throws MyException{
        Arithmetic obj = new Arithmetic(5,5);
        assertEquals(10,obj.calculateSum());
    }
    @Test(expected = MyException.class )
    public void testMyException() throws MyException{
        Arithmetic obj2 = new Arithmetic(9999,9999);
    }

    @Test
    public void VerifyTrue() throws MyException{
        Arithmetic obj2 = new Arithmetic(100,99);
        assertTrue(obj2.compare());
    }

    @Test
    public void VerifyFalse() throws MyException{
        Arithmetic obj3 = new Arithmetic(100,399);
        assertFalse(obj3.compare());
    }
}
