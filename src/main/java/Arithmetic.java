/*Some functionality to test with JUnit */
public class Arithmetic {
    int var1,var2;

    public Arithmetic(int var1, int var2) throws MyException{
        if(var1>4563 || var2>4563){
            throw new MyException("Out of bound exception!");
        }
        this.var1 = var1;
        this.var2 = var2;
    }

    public int getVar1() {
        return var1;
    }

    public Arithmetic() {
    }

    public void setVar1(int var1) throws MyException {
if(var1>4563){
    throw new MyException("Out of bound exception!");
}
        this.var1 = var1;
    }

    public int getVar2() {
        return var2;
    }

    public void setVar2(int var2) throws MyException {
        if(var2>4563){
            throw new MyException("Out of bound exception!");
        }
        this.var2 = var2;
    }

    public int calculateSum(){
        return (this.var1+this.var2);
    }
    public boolean compare(){
        if(var1>var2){return true;}
        else return false;
    }

    }

