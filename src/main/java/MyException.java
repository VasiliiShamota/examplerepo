public class MyException extends Exception {

    MyException(String message){
        super(message);
    }

    @Override
    public String toString() {
        return "MyException{}";
    }
}
